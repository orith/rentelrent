﻿
<!DOCTYPE html>
<html class=" js flexbox flexboxlegacy canvas canvastext webgl no-touch geolocation postmessage no-websqldatabase indexeddb hashchange history draganddrop websockets rgba hsla multiplebgs backgroundsize borderimage borderradius boxshadow textshadow opacity cssanimations csscolumns cssgradients no-cssreflections csstransforms csstransforms3d csstransitions fontface generatedcontent video audio localstorage sessionstorage webworkers applicationcache svg inlinesvg smil svgclippaths" lang="en"><head>
<meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="description" content="responsive creative template">
    <meta name="keywords" content="portfolio, personal, corporate, business, parallax, creative, agency,team portfolio,team">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:image" content="search.png">

    <!-- <title>Rentster | Trusted Accomodation Site</title> -->
    <title>Rom, Hus, Leilighet, Leilighet, Studio, Sublite, Partner  </title>
    <link href="css.css" rel="stylesheet">
    <link href="all.css" rel="stylesheet">
    <link rel="stylesheet" href="bootstrap.css">
    <link rel="stylesheet" href="normalize.css">
    <link rel="stylesheet" href="style.css">
    <link rel="stylesheet" href="responsive.css"> 
    <link href="https://www.aparttmenttrent.cf/favicon.html" rel="shortcut icon" type="image/x-icon">
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
</head>

<body>
    <!-- Header-Area Start -->
    <div class="header-are">
        <div class="main-menu">
            <nav class="navbar navbar-default">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <div class="logo">
                            <a class="navbar-brand" href="https://www.roomster.com/login?r=0000e113"><img src="logo.png" alt=""></a>
                        </div>
                    </div>
                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav navbar-right">
                            <li class="social-icon"><a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-google-play"></i></a></li>
                            <li class="social-icon"><a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-apple"></i></a></li>
                            <li class="social-icon"><a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-amazon"></i></a></li>
                            <li class="dropdown">
                                <a href="https://www.roomster.com/login?r=0000e113" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Engelsk <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                          
                                    <li><a href="https://www.roomster.com/login?r=0000e113">spansk</a></li>
                                </ul>
                            </li>
                            <li id="fb-login"><a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-facebook-f"></i>adgang</a></li>
                            <li id="help"><a href="https://www.roomster.com/login?r=0000e113">hjelpe<i class="fas fa-question-circle"></i></a></li>
                        </ul>
                    </div>
                    <!-- /.navbar-collapse -->
                </div>
                <!-- /.container-fluid -->
            </nav>
        </div>
        <div class="background-area">
            <div class="overlay"></div>
            <div class="container">
                <div class="row">
                    <div class="col-md-3"></div>
                    <div class="col-md-7">
                        <div class="slider-text">
                            <h1 class="text-align-center">Hvor enn du er <span>192 land</span> &amp; <span>18 språk.</span></h1>
                            <div class="slider-btn">
                                <div class="title">
                                Jeg ser etter
                                </div>
                                <div class="flex-container">
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-home"></i>rom</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-user"></i>Romkamerat</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-home"></i>leilighet</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-bed"></i>studio</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-user"></i>fremleie</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-bed"></i>utleier</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="far fa-building"></i>Flat andel</a></li>
                                    <li><a href="https://www.roomster.com/login?r=0000e113"><i class="far fa-building"></i>samboer</a></li>
									<li><a href="https://www.roomster.com/login?r=0000e113"><i class="fa fa-home"></i>studenter</a></li>
                                </div>                               
                            </div>
                        </div>
                        <div class="scrool-down">
                            <div id="gdb1" class="gdbutton fontwhitenshadow">
                                <span><i aria-hidden="true" class="fa fa-angle-down"></i></span>
                            </div>
                        </div>
                        <div class="col-md-2"></div>
                        <!-- column End -->
                    </div>
                    <!-- row End -->
                </div>
                <!-- container End -->
            </div>
        </div>
    </div>
    <!-- Header-Area End -->

    <!-- About-Area Start -->
    <div class="about-are sp-60">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <div class="single-about">
                        <div class="ab-icon">
                            <i class="fas fa-users"></i>
                            <h4>Finn flotte mennesker</h4>
                            <p>Kommuniser med brukerne dine via en postkasse <span>Rentster & sosial kontakt.</span></p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-about">
                        <div class="ab-icon">
                            
                            <i class="fas fa-home"></i>
                            <h4>Finn et hjem</h4>
                            <p>Søk etter leie i 192 land og 18 språk..</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="single-about">
                        <div class="ab-icon">
                            <i class="fas fa-subway"></i>
                            <h4>Godta kjæledyr</h4>
                            <p>Bruk søkefilter for å finne hjem og mennesker som tar imot dyr.</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="more-about">
                <a href="https://www.roomster.com/login?r=0000e113">Mer informasjon om Rentster</a>
            </div>
        </div>
    </div>
    <!-- About-Area End -->

    <!-- Service-Area Start -->
    <div class="service-area sp-60">
        <div class="container">
            <div class="row">
                <div class="col-md-6">
                    <div class="service-btm-left">
                        <h1><a href="https://www.roomster.com/login?r=0000e113">Liste over plassen din</a></h1>
                        <p>Tjen penger ved å leie ledig plass.  Fullfør drømmene dine!</p>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="service-btm-right">
                        <h1><a href="https://www.roomster.com/login?r=0000e113">Rentster Sosial tilknytning</a></h1>
                        <p>Chat med utleiemedlemmene dine via ditt favoritt sosiale nettverk.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Service-Area End -->

    <!-- Footer-Area Start -->
    <div class="footer-area sp-60">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="footer-logo">
                        <a href="https://www.roomster.com/login?r=0000e113"><img src="logo.png" alt=""></a>
                    </div>
                </div>
            </div>
            <hr>
            <div class="footer-top">
                <div class="row">
                    <div class="col-md-5">
                        <div class="footer-widget-1">
                            <div class="importent-link">
                                <li><a href="https://www.roomster.com/login?r=0000e113">utmerkelser</a></li>
                                <li><a href="https://www.roomster.com/login?r=0000e113">Team Rentster</a></li>
                                <li><a href="https://www.roomster.com/login?r=0000e113">Kontakt ønsker oss</a></li>
                            </div>
                            <div class="social-link">
                                <li><a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-facebook-f"></i></a>
                                    <a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-twitter"></i></a>
                                    <a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-google-plus-g"></i></a>
                                    <a href="https://www.roomster.com/login?r=0000e113"><i class="fab fa-instagram"></i></a>
                                </li>

                            </div>  
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="footer-widget-2">
                            <li><a href="https://www.roomster.com/login?r=0000e113">postkasse</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">megafon</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">markører</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">Brukerstøtte til 24/7</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">innstillinger</a></li>
                        </div> 
                    </div>  
                    <div class="col-md-4">
                        <div class="footer-widget-3">
                            <li><a href="https://www.roomster.com/login?r=0000e113">lekse</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">Hjelpesenter</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">FAQ</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">Like muligheter for overnatting</a></li>
                            <li><a href="https://www.roomster.com/login?r=0000e113">Tilknyttet program</a></li>

                        </div>       
                    </div>   
                </div>
            </div>
            <hr>    
            <div class="footer-bottom">
                <div class="col-md-5">
                    Rentster &amp; kopiere 2018
                </div>
                <div class="col-md-4">
                    <a href="#">Vilkår for bruk </a> 
                </div>
                <div class="col-md-2">
                    <a href="#">Personvern </a>
                </div>
            </div> 
        </div>
    </div>
    <!-- Footer-Area End -->
    <script src="jquery_002.js"></script>     
    <script src="modernizr.js"></script>
    <script src="bootstrap.js"></script>
    <script src="jquery.js"></script>
    <script src="main.js"></script>


</body></html>
"
